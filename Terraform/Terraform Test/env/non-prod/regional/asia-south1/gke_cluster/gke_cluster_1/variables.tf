/* Cluster Variables */

variable "project_id" {
  description = "The project ID to host the cluster in"
  type        = string
}


variable "region" {
  description = "The region to host the cluster in"
  type        = string
}

variable "node_pools" {
  description = "node pools specifications"
}

variable "cluster_name" {
  type        = string
  description = "The name of the cluster (required)"
}

variable "description" {
  type        = string
  description = "The description of the cluster"
  default     = ""
}

variable "zones" {
  type        = list(string)
  description = "The zone to host the cluster in (required if is a zonal cluster)"
  #default     = "asia-south1-a"
}

variable "network" {
  description = "The VPC network to host the cluster in"
  type        = string
}

variable "subnetwork" {
  description = "The subnetwork to host the cluster in"
  type        = string
}

variable "ip_range_pods" {
  description = "The secondary ip range to use for pods"
}

variable "ip_range_services" {
  description = "The secondary ip range to use for services"
}

variable "service_account" {
  description = "Service account to associate to the nodes in the cluster"
}

variable "master_ipv4_cidr_block" {
  type        = string
  description = "(Beta) The IP range in CIDR notation to use for the hosted master network"
  default     = "10.0.0.0/28"
}

variable "master_authorized_networks" {
  type        = list(object({ cidr_block = string, display_name = string }))
  description = "List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists)."
  default     = []
}

variable "kubernetes_version" {
  type        = string
  description = "The Kubernetes version of the masters. If set to 'latest' it will pull latest available version in the selected region."
  default     = "latest"
}

variable "regional" {
  type    = bool
  default = true
}

variable "max_pods" {
  description = "Maximum number of pods per nodes."
  default     = 110
}

variable "cluster_autoscaling" {
  type = object({
    enabled             = bool
    autoscaling_profile = string
  })
  default = {
    enabled             = false
    autoscaling_profile = "BALANCED"
  }
  description = "Cluster autoscaling configuration. See [more details](https://cloud.google.com/kubernetes-engine/docs/reference/rest/v1beta1/projects.locations.clusters#clusterautoscaling)"
}

variable "gke_cluster_min_master_version" {
  default = "1.20.6-gke.1000"
}

variable "local_ssd_count" {
  default = 0
}

variable "enable_autoscaling" {
  type    = bool
  default = true
}

variable "windows_node_count" {
  description = "number of nodes in the NodePool."
  default     = 1
}

variable "windows_min_node_count" {
  description = "Minimum number of nodes in the NodePool."
  default     = 1
}

variable "windows_max_node_count" {
  description = "Maximum number of nodes in the NodePool."
  default     = 3
}

variable "windows_machine_type" {
  description = "The name of a Google Compute Engine machine type"
  default     = "n1-standard-2"
}

variable "windows_image_type" {
  description = "The image type to use for this node."
  default     = "COS"
}

variable "windows_disk_type" {
  description = "Type of the disk attached to each node (e.g. 'pd-standard' or 'pd-ssd')"
  default     = "pd-standard"
}

variable "windows_disk_size_gb" {
  description = "Size of the disk attached to each node, specified in GB. The smallest allowed disk size is 10GB"
  default     = "100"
}

variable "gce_labels" {
  default = ""
}


# variable "linux_node_pool_name" {
#   type        = string
#   description = "The name of the node pool (required)"
# }

# variable "linux_kubernetes_labels" {
#   default = ""
# }

# variable "linux_node_count" {
#   description = "number of nodes in the NodePool."
#   default     = 1
# }

# variable "enable_shielded_nodes" {
#   type        = bool
#   description = "Enable shielded nodes in gke cluster"
#   default     = true
# }

# variable "enable_secure_boot" {
#   default = "true"
# }  

# variable "network_policy" {
#   type        = bool
#   description = "Enable network policy addon"
#   default     = true
# }

# variable "gcp_filestore_csi_driver_config" {
#   type        = bool
#   description = "Enable network policy addon"
#   default     = true
# }

# variable "linux_min_node_count" {
#   description = "Minimum number of nodes in the NodePool."
#   default     = 1
# }

# variable "linux_max_node_count" {
#   description = "Maximum number of nodes in the NodePool."
#   default     = 3
# }

# variable "linux_machine_type" {
#   description = "The name of a Google Compute Engine machine type"
#   default     = "n1-standard-2"
# }

# variable "linux_image_type" {
#   description = "The image type to use for this node."
#   default     = "COS"
# }

# variable "linux_disk_type" {
#   description = "Type of the disk attached to each node (e.g. 'pd-standard' or 'pd-ssd')"
#   default     = "pd-standard"
# }

# variable "linux_disk_size_gb" {
#   description = "Size of the disk attached to each node, specified in GB. The smallest allowed disk size is 10GB"
#   default     = "100"
# }

# variable "linux_max_pods" {
#   description = "Maximum number of pods per nodes."
#   default     = 110
# }

# variable "maintenance_end_time" {
#   type        = string
#   description = "Time window specified for recurring maintenance operations in RFC3339 format"
#   default     = ""
# }

# variable "maintenance_recurrence" {
#   type        = string
#   description = "Frequency of the recurring maintenance window in RFC5545 format."
#   default     = ""
# }

# variable "maintenance_start_time" {
#   type        = string
#   description = "Time window specified for daily or recurring maintenance operations in RFC3339 format"
#   default     = ""
# }
