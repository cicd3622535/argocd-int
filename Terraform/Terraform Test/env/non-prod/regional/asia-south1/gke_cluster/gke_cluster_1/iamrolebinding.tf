resource "google_project_iam_binding" "secret_manager_binding_01" {
  project     = "clean-abacus-393308"
  role        = "roles/secretmanager.admin"
  members     = ["user:shekharayushi1001@gmail.com"]
}

resource "google_project_iam_binding" "secret_manager_binding_02" {
  project     = "clean-abacus-393308"
  role        = "roles/container.clusterAdmin"
  members     = ["user:shekharayushi1001@gmail.com"]
}

resource "google_project_iam_binding" "secret_manager_binding_03" {
  project     = "clean-abacus-393308"
  role        = "roles/iam.serviceAccountAdmin"
  members     = ["user:shekharayushi1001@gmail.com"]
}

resource "google_service_account" "service_account_read" {
  account_id   = "readonly-secrets"
  display_name = "Read secrets"
}

resource "google_service_account" "service_account_write" {
  account_id   = "readwrite-secrets"
  display_name = "Read write secrets"
}

resource "google_project_iam_binding" "secret_manager_binding" {
  project     = "clean-abacus-393308"
  role        = "roles/secretmanager.secretAccessor"
  members     =  ["serviceAccount:readonly-secrets@clean-abacus-393308.iam.gserviceaccount.com"]
}


resource "google_project_iam_binding" "secret_manager_binding_write" {
  project     = "clean-abacus-393308"
  role        = "roles/secretmanager.secretAccessor"
  members     = ["serviceAccount:readwrite-secrets@clean-abacus-393308.iam.gserviceaccount.com"]
}

resource "google_project_iam_binding" "secret_manager_binding_version_add" {
  project     = "clean-abacus-393308"
  role        = "roles/secretmanager.secretVersionAdder"
  members     = ["serviceAccount:readwrite-secrets@clean-abacus-393308.iam.gserviceaccount.com"]
}





