/* Variable's Values */

/* GLOBAL */

project_id = "clean-abacus-393308"
region     = "asia-south1"
zones      = ["asia-south1-a", "asia-south1-b"]

/* Network part */
network           = "default"
subnetwork        = "default"
ip_range_pods     = "pod-range"
ip_range_services = "svc-range"


/* Cluster Configuration */
cluster_name                   = "demo-gke-cluster-1"
master_ipv4_cidr_block         = "10.149.25.0/28"
kubernetes_version             = "1.27.3-gke.100"
gke_cluster_min_master_version = "1.27.3-gke.100"
max_pods                       = 20
master_authorized_networks = [
  {
    cidr_block   = "10.160.0.29/32"
    display_name = "bastion server access"
  }

  # { cidr_block   = ""
  #   display_name = "VM Access"
  # },
]

/* Maintenance window configuration */

# maintenance_recurrence = "FREQ=WEEKLY;BYDAY=SA,SU"
# maintenance_start_time = "2023-06-23T23:59:00Z"
# maintenance_end_time   = "2023-06-25T01:00:00Z"

# a regional or a zonal cluster
regional = false

service_account = "28734378954-compute@developer.gserviceaccount.com"

/* Node Pool Configuration */

node_pools = [
  {
    linux_node_pool_name = "gke-node-pool"
    linux_disk_size_gb   = 50
    linux_disk_type      = "pd-balanced"
    linux_image_type     = "COS_CONTAINERD"
    linux_machine_type   = "e2-medium"
    linux_min_node_count = 1
    linux_max_node_count = 2
    linux_max_pods       = 20
    linux_node_count     = 1
    auto_upgrade         = true
    auto_repair          = true
    preemptible          = false
    linux_kubernetes_labels = {
      env = "nonprod"
    }
  },
]
