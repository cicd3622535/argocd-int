resource "google_service_account" "readonly-secrets" {
  account_id   = "readonly-secrets"
  
}

module "my-app-workload-identity" {
  source              = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  use_existing_gcp_sa = true
  name                = google_service_account.readonly-secrets.account_id
  project_id          = "clean-abacus-393308"
  annotate_k8s_sa     = true


  # wait for the custom GSA to be created to force module data source read during apply
  # https://github.com/terraform-google-modules/terraform-google-kubernetes-engine/issues/1059
  depends_on = [google_service_account.readonly-secrets]
}

resource "google_service_account" "readwrite-secrets" {
  account_id   = "readwrite-secrets"
 
}

module "my-app-workload-identity_01" {
  source              = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  use_existing_gcp_sa = true
  name                = google_service_account.readwrite-secrets.account_id
  project_id          = "clean-abacus-393308"
  annotate_k8s_sa     = true
  

  # wait for the custom GSA to be created to force module data source read during apply
  # https://github.com/terraform-google-modules/terraform-google-kubernetes-engine/issues/1059
  depends_on = [google_service_account.readwrite-secrets]
}