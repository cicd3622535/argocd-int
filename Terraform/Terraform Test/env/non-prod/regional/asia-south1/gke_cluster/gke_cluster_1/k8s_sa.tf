resource "kubernetes_service_account_v1" "readonly-sa" {
  metadata {
    name = "readonly-sa"
    namespace = "readonly-ns"
   
  }
}


resource "kubernetes_service_account_v1" "admin-sa" {
  metadata {
    name = "admin-sa"
    namespace = "admin-ns"
    
  }
}
