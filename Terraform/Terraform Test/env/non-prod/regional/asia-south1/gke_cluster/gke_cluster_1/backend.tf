terraform {
  backend "gcs" {
    bucket = "ayushi-demo-tf-bucket"
    prefix = "env/prod/regional/asia-south1/gke_cluster"
  }
}
