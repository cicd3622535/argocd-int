# Global Configurations

data "google_client_config" "default" {}

data "google_project" "project" {
  project_id = var.project_id
}

# Cluster configurations

module "gke_private_cluster" {
  source                        = "../../../../../../modules/google_kubernetes_engine/gke_private_cluster"
  project_id                    = var.project_id
  name                          = var.cluster_name
  region                        = var.region
  network                       = var.network
  subnetwork                    = var.subnetwork
  ip_range_pods                 = var.ip_range_pods
  ip_range_services             = var.ip_range_services
  regional                      = var.regional
  zones                         = var.zones
  http_load_balancing           = true
  network_policy                = false
  logging_service               = "logging.googleapis.com/kubernetes"
  monitoring_service            = "monitoring.googleapis.com/kubernetes"
  horizontal_pod_autoscaling    = true
  enable_private_endpoint       = true
  enable_private_nodes          = true
  deploy_using_private_endpoint = true
  enable_shielded_nodes         = true
  master_ipv4_cidr_block        = var.master_ipv4_cidr_block
  remove_default_node_pool      = true
  service_account               = var.service_account
  kubernetes_version            = var.kubernetes_version
  cluster_resource_labels = {
    "env" : "nonprod"
  }
  enable_binary_authorization = false
  workload_identity_namespace = "${var.project_id}.svc.id.goog"
  master_authorized_networks  = var.master_authorized_networks
  # maintenance_recurrence      = var.maintenance_recurrence
  # maintenance_start_time      = var.maintenance_start_time
  # maintenance_end_time        = var.maintenance_end_time
}

# Node-pool Configurations


module "gke_linux_node_pool" {
  source                         = "../../../../../../modules/google_kubernetes_engine/gke_node_pool"
  count                          = length(var.node_pools)
  project_id                     = var.project_id
  gke_cluster_name               = module.gke_private_cluster.name
  node_pool_name                 = var.node_pools[count.index]["linux_node_pool_name"]
  region                         = var.region
  regional                       = var.regional
  zones                          = module.gke_private_cluster.zones
  gke_cluster_min_master_version = var.gke_cluster_min_master_version
  image_type                     = var.node_pools[count.index]["linux_image_type"]
  machine_type                   = var.node_pools[count.index]["linux_machine_type"]
  preemptible                    = var.node_pools[count.index]["preemptible"]
  auto_upgrade                   = var.node_pools[count.index]["auto_upgrade"]
  auto_repair                    = var.node_pools[count.index]["auto_repair"]
  max_pods_per_node              = var.node_pools[count.index]["linux_max_pods"]
  node_count                     = var.node_pools[count.index]["linux_node_count"]
  local_ssd_count                = "0"
  enable_autoscaling             = var.enable_autoscaling
  min_node_count                 = var.node_pools[count.index]["linux_min_node_count"]
  max_node_count                 = var.node_pools[count.index]["linux_max_node_count"]
  labels                         = var.node_pools[count.index]["linux_kubernetes_labels"]
  disk_size_gb                   = var.node_pools[count.index]["linux_disk_size_gb"]
  disk_type                      = var.node_pools[count.index]["linux_disk_type"]
  service_account                = var.service_account
  enable_secure_boot             = true
}

