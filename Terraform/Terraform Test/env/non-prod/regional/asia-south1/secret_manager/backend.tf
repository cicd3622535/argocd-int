// file: backend.tf

terraform {
  backend "gcs" {
    bucket = "ayushi-demo-tf-bucket" #your bucket name
    prefix = "prod/global/secret_manager/simple"
  }
}
