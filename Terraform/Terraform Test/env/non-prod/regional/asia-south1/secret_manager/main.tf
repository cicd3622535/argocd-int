# This file demonstrates applying user-defined replication options with Secret Manager

module "secret" {
  source     = "../../../../../modules/secret_manager/user_provided/"
  project_id = var.project_id
  id         = var.id
  secret     = var.secret
  labels     = var.labels
}
