project_id = "clean-abacus-393308"
region     = "asia-south1"
buckets = [
/* Bucket Configuration */
{
  name               = "ayushi-demo-tf-bucket"
  location           = "asia-south1"
  storage_class      = "STANDARD"
  versioning         = true
  force_destroy      = false
  labels             = { env = "dev" }
  iam_members        = [
  {  role = "roles/storage.objectViewer",
     member = "user:abc"
  }
  ]
  lifecycle_rules    = [
  {
    condition = {
      age = 90
    }
    action = {
      type = "Delete"
    }
  }  
  ]
  bucket_policy_only = true
  uniform_bucket_level_access = true #false
}
]




