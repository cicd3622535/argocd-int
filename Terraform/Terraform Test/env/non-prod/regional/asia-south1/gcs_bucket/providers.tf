provider "google" {
  project = var.project_id
  region  = var.region
}

# output "searce-gcs-bucket" {
#   description = "Bucket resource"
#   value       = module.searce-gcs-bucket.*.bucket
# }


# output "searce-gcs-bucket_name" {
#   description = "Bucket name"
#   value       = module.searce-gcs-bucket.*.bucket.name
# }


# output "searce-gcs-bucket_url" {
#   description = "Bucket URL"
#   value       = module.searce-gcs-bucket.*.bucket.url
# }
